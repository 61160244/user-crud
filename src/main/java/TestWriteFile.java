
import java.io.File;
import java.io.FileNotFoundException;
import java.io.ObjectOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.tools.FileObject;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Windows
 */
public class TestWriteFile {
    public static void main(String[] args) {
        FileOutputStream fos = null;
        File file = null;
        ObjectOutputStream oos = null;
        User user1 = new User("test1", "test1");
        User user2 = new User("test2", "test2");
        ArrayList<User> list = new ArrayList();
        list.add(user1);
        list.add(user2);
        try {
            file = new File("test.bin");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(list);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(TestWriteFile.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TestWriteFile.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                oos.close();
                fos.close();
            } catch (IOException ex) {
                Logger.getLogger(TestWriteFile.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }
    
    
}
