
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Windows
 */
public class UserService {

    public static ArrayList<User> list = new ArrayList();

    public static ArrayList<User> getList() {
        return list;
    }
    
    public static void load() {
        File file = null;
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        ArrayList<User> user = new ArrayList();
        try {
            file = new File("User.bin");
            fis = new FileInputStream(file);
            ois = new ObjectInputStream(fis);

            user = (ArrayList<User>) ois.readObject();

            for (User u : user) {
                list.add(u);
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(TestReadFile.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TestReadFile.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(TestReadFile.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void save(ArrayList<User> list) {
        FileOutputStream fos = null;
        File file = null;
        ObjectOutputStream oos = null;
        UserService.list = list;
        try {
            file = new File("User.bin");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(list);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(TestWriteFile.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                oos.close();
                fos.close();
            } catch (IOException ex) {
                Logger.getLogger(TestWriteFile.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    public static boolean auth(String username, String password) {
        for (User user : list) {
//            System.out.println(user.getUsername());
//            System.out.println(1);
            if (user.getUsername().equals(username) && user.getPassword().equals(password)) {
                return true;
            }
        }
//        System.out.println(1);
        return false;
    }
}
