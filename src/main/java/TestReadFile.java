/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Windows
 */
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.management.ObjectInstance;
public class TestReadFile {
    public static void main(String[] args) {
        File file = null;
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        ArrayList<User> user = new ArrayList();
        try {
            file = new File("test.bin");
            fis = new FileInputStream(file);
            ois = new ObjectInputStream(fis);
            
            user = (ArrayList<User>)ois.readObject();
            
            for(User u : user){
                System.out.println(u);
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(TestReadFile.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TestReadFile.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(TestReadFile.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
